const gameList = [
  {
    name: 'Kleptocats',
    url: 'https://hyperbeard.com/game/kleptocats/',
  },
  {
    name: 'Chichens',
    url: 'https://hyperbeard.com/game/chichens/',
  },
  {
    name: 'Alchademy',
    url: 'https://hyperbeard.com/game/alchademy/',
  },
  {
    name: 'The Balloons',
    url: 'https://hyperbeard.com/game/the-balloons/',
  },
  {
    name: 'Muertitos',
    url: 'https://hyperbeard.com/game/muertitos/',
  },
  {
    name: 'Palabraz',
    url: 'https://hyperbeard.com/game/palabraz/',
  },
  {
    name: 'Bouncing Dudde',
    src: 'https://hyperbeard.com/game/bouncing-dude/',
  },
  {
    name: '8Puzzle',
    url: '/games/8puzzle/',
  },
  {
    name: 'Aging',
    url: '/games/aging/',
  },
  {
    name: 'Asecan',
    url: '/games/asecan/',
  },
  {
    name: 'Bike',
    url: '/games/bike/',
  },
  {
    name: 'CRSG',
    url: '/games/crsg/',
  },
  {
    name: 'DTL',
    url: '/games/dropTheLight/',
  },
  {
    name: 'GTW',
    url: '/games/goToWork/',
  },
  {
    name: 'Jokes on you',
    url: '/games/standUp/ ',
  },
  {
    name: 'GGJ16',
    url: '/games/ggj/',
  },
];

module.exports = gameList;
