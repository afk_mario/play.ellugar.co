module.exports = {
  globals: {
    STATIC_PATH: true
  },
  parser: 'babel-eslint',
  extends: 'airbnb-base',
  env: {
    browser: true,
  },
};
